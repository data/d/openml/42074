# OpenML dataset: wine_reviews

https://www.openml.org/d/42074

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Wine data gathered by https://www.kaggle.com/zynicideThe data was scraped from WineEnthusiast during the week of June 15th, 2017. The code for the scraper can be found at https://github.com/zackthoutt/wine-deep-learning

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42074) of an [OpenML dataset](https://www.openml.org/d/42074). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42074/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42074/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42074/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

